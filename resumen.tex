\documentclass[conference]{IEEEtran}

% Spanish
\usepackage[english]{babel}

% Math
\usepackage{amsmath}
\usepackage{amsfonts}

\usepackage{multirow}

\usepackage{todonotes}

\usepackage[
citestyle=ieee
]{biblatex}
\addbibresource{referencias.bib}

%\usepackage[a4paper]{geometry}

\title{Optical Flow of Moving Particles: a Review} 

\author{\IEEEauthorblockN{Emmanuel Madrigal\IEEEauthorrefmark{1}}
\IEEEauthorblockA{Electronics School,
Costa Rica Institute of Technology\\
Costa Rica\\
Email: \IEEEauthorrefmark{1}emmadrigal@estudiantec.cr}}


\begin{document}

\maketitle

% https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=8627998

\begin{abstract}
	Optical flow is one of the fundamental computer vision tasks, traditional
	techniques such as Lucas Kanade, deep learning methods such as
	FlowNet and unsupervised methods such as UnFlow have been proposed
	to solve this problem.

	Movement in particles and flows has traditionally been studied by
	using optical techniques such as Particle Image Velocimetry (PIV) which provide a sparse optical
	flow, and over the last few years optical flow techniques have
	started to make headway into this task.

	This paper explores the current state-of-the-art techniques for
	optical flow calculation using computer vision techniques and from a
	particle study point of view and show how the particle and flows
	studies are lagging behind in the usage of ``modern'' optical flow
	methods might improve the quality of the results.
\end{abstract}
\begin{IEEEkeywords} Optical Flow, CNN, Unsupervised Learning, PIV, Particle Tracking \end{IEEEkeywords}

\section{Introduction}
% An ‘Introduction” should be included to explain to the reader the
% background to the subject area (how it has developed historically
% and its importance to the modern industrial world). The section
% should end by explaining why the author(s) have written the paper
% and how they have dealt with the subject area

Optical flow is a computer vision task with a wide variety of
applications and serves as a building block for more general
systems. This task consists on receiving a pair of sequential images
and giving as an output the movement vector for some or all pixels in
the image.

Traditionally this task has been formulated as an optimization problem
in which correspondences between two video frames are
looked for.

With the recent development of deep convolutional networks and their
success in solving other computer vision
tasks~\cite{ren2015faster}~\cite{he2017mask}~\cite{krizhevsky2012imagenet},
deep learning methods have been adapted to estimate the optical
flow. These techniques have benefited from the large availability of
annotated datasets, however, since obtaining the real-world annotated
data for this task is time-consuming most of these are based on
synthetic data or focused on a particular scene.

Unsupervised methods have arisen to face these tasks, reformulating
the task as a reconstruction between two sequential frames. Instead of
comparing the output against a set pre-annotated labels, a loss
function is established and uses the optical flow result to warp the
input image, that is to try and recreate the output frame and compares
it against the actual next frame.

The study particle and flow movements has been generally focused on
optical methods such as Particle Image Velocity
(PIV)~\cite{Willert1991}, Bubble Image Velocimetry
(BIV)~\cite{Ryu2005}, and Particle Tracking Velocimetry
(PTV)~\cite{Maas1993}, rather than using computer vision
techniques. In the last few years research has also started on
application of more classical optical flow methods for this
area~\cite{Abrishami2015}~\cite{Rossi2019}~\cite{Lin2019}.

In section \ref{sec:optical_flow} state-of-the-art methods for optical
flow are highlighted. Section \ref{sec:datasets} shows the currently
available datasets and the corresponding benchmarks. Section
\ref{sec:particles_and_flow} shows methods methods currently being used
to study particles and flow. Finally, sections\ref{sec:conclusions}
provide the conclusions and the future work that needs to be done in
this area.

% Could also be divided by which problem they are tring to solve
\section{Optical Flow}
\label{sec:optical_flow}

\subsection{Traditional Methods}
% Generic methods

The traditional methods for determining the the optical flow are the
Lucas-Kanade (LK)~\cite{lucas1981iterative} and the Horn-Schunck
(HS)~\cite{horn1981determining} algorithms. Although both of these are
almost 40 years old, thanks to iterations and optimizations over these
algorithms~\cite{Bouguet00pyramidalimplementation}, the techniques
they initially proposed still remain useful for many tasks.

Some of the challenges that these techniques face are inaccuracy when
trying to predict large displacement of small or thin objects,
vulnerability to noise and, discontinuities such as
occlusions~\cite{Baker2011}~\cite{fortun2015optical}. They also make suppositions on the
composition of the image such as brightness constancy and spacial
smoothness which are only an approximation to reality~\cite{Yu2016}.

One of the advantages of this methods is that their accuracy can be
improved by performing iterations as follows:

\begin{enumerate}
	\item Get an initial approximation of the flow.
	\item Warp the starting image to match the flow.
	\item Perform step 1. comparing the warped image and the resulting image.
\end{enumerate}

Which can be repeated to reduce errors but incurs a large
computational load.

A fast optical flow algorithm that can handle large displacement
motions is proposed in~\cite{Bao2014}, this is a fast nearest neighbor
algorithm which propagates similarity patterns and offsets allowing
for a better matching.

\cite{Kroeger2016}~proposes dividing the image into patches and an
inverse search is performed to obtain matches to the ``starting''
image. This is then evaluated in multiple scales to determine if
possible correspondences exist not only for small displacements but
also for large. These large displacements are ones of the main
challenges that classic methods face.

\subsection{Supervised Methods}
% Generic methods

Convolutional neural networks (CNN) have advanced the field of
computer vision in recent years, such as image classification, object
detection and image segmentation.

Starting with the work done
by~\cite{DBLP:journals/corr/FischerDIHHGSCB15} called FlowNet which
proposes two CNN models for optical flow, trained based on a synthetic
dataset and running and an end-to-end network. This initial proposal
still had a worse performance than the traditional methods specially
on real world data due to its training using synthetic data. This
algorithm was later refined in FlowNet2.0~\cite{Ilg2017} and extended
into several sequential networks, where one of the main findings was
that the schedule in which the training data is presented has a large
impact on the network performance. Large redundancies in the network
architecture were also addressed by LiteFlowNet~\cite{Hui2018},
reducing the network size by around a factor of 30 times, improving
speed by $1.36$ while still keeping the performance on par. This work
is also is similar to TVNet~\cite{Fan2018}.

Other networks such as PWC-Net have also implement techniques proposed
by traditional methods: pyramidal processing, warping and the use of a
cost version~\cite{Sun2018}, which allows it to outperform the
end-to-end network.

Other deep learning techniques have been applied in other
approaches. One of these is the usage of dense networks such as in
DenseNet~\cite{Zhu2018}. Which has all layers of the network connected
between them, this means that the output is able to work from both
large and small scale features to predict their output. Similar work
is also replicated in SPyNet~\cite{Ranjan2016} focused in a
coarse-to-fine approach, that is finding large-scale features and the
refining these into fine-grained values.

Improvements in the training schedule were further studied by
ScopeFlow~\cite{Bar-Haim2020}, they were able to improve the
performance of PWC-Net by $10\%$ in the Sintel benchmark.

Data augmentation was explored by SelFlow~\cite{Liu2019} by creating
occlusions based on superpixels in order to increase the resilience of
the network against of these obstacles, it also uses information from
several frames rather than a only two in order to improve performance.

The form of the output vector has been studied by~\cite{Yang2019}, it
uses volumetric encoder-decoder, multi-channel cost volumes and
separable volumetric filtering. Which leads to both a higher
performance and a faster convergence.

\subsection{Unsupervised Methods}
% Generic methods

Although the state-of-the-art methods in terms of performance are
given by supervised networks these require large amounts of training
data in order to achieve these level of performance, and the resulting
network will be biased to the the scenario represented by the training
data. Generating new training data for new scenarios can often be
expensive and time consuming. Unsupervised methods bypass the labeling
stage by representing the optical flow task as a reconstruction task,
and comparing the current reconstruction given by the network against
the expected following frame, and using it as the loss function.

The flow function proposed initially in~\cite{Yu2016} is similar to
objective functions proposed for two-frame motion estimation; however
rather than optimize the velocity map between input frames, it is used
to optimized the CNN weights.

UnFlow~\cite{Meister2017} proposes their loss function based on a
bidirectional flow estimation and census transform to represent the
loss function.

Since during reconstruction there will be a fraction of pixels in the
target image that have no source pixels due to occlusion, the accuracy
of the loss function is reduced. The cause of this is that a
reconstruction cannot be performed on this missing information. To
address this issue~\cite{Wang2018} estimates the optical flow
backwards and uses it to generate an occlusion map for the warped
frame, the network is only trained to reconstruct the non-occluded
area and avoiding false positives from occluded areas.

GeoNet~\cite{Yin2018} follows a different approach where a 3D
geometric representation of the scene is generated which is further
refined to obtain the monocular depth, the optical flow and global motion
estimation from the video.

DFNet~\cite{Zou2018} also leverages geometric consistency, that is it
assumes that the rigid regions can be used to determine the scene
depth and the camera motion and projecting the 3D scene to synthesize a
2D optical flow.

\section{Available Datasets}
\label{sec:datasets}

Publicly available datasets have been responsible for large advances in
the performance of supervised training of neural networks. Some of
these also contain an accompanying public benchmark which allows the
comparison of the performance of these networks.

\subsection{Flying Chairs}
Flying Chairs is a synthetic dataset created by superimposing images
of chairs on background images from Flickr. It was originally created
by FlowNet~\cite{DBLP:journals/corr/FischerDIHHGSCB15}. It is
regularly used as an initial stage in training.

\subsection{Sintel}
Sintel~\cite{Butler} is a dataset of around 1000 image pairs, this is
based on a 3D annotated short film which displays a large range of
``natural'' motions. This is also a synthetic dataset which
means that good performance on this dataset doesn't translate directly
into good performance on real world scenarios.

\subsection{KITTI}
KITTI~\cite{Geiger2013} is based on recordings from real world driving
conditions, of which only a section is labeled which provides an
advantage for unsupervised methods. Two sets have been published and
each is named by the year (2012 and 2015). In which the latter one has
a greater variety of scenes including pedestrians and road signs.

\subsection{Middlebury}
Middlebury~\cite{Baker2011} is a stereo images dataset meant for depth
algorithms, however the depth can also be represented as a 3D
reconstruction which can be also be used as an output for an optical flow
network.

% Please add the following required packages to your document preamble:
% \usepackage{multirow}
\begin{table*}[htbp]\centering
	\begin{tabular}{|l|l|l|l|l|l|l|l|}
		\hline
		                              & Method                                               & Chairs        & Sintel        & KITTI-2012   & KITTI-2015    & Middlebury    & Time (ms)   \\ \hline
		Traditional                   & Lucas-Kanade~\cite{Bouguet00pyramidalimplementation} &               &               & 33.2         & 72.67         &               & 90000       \\ \hline
		\multirow{10}{*}{Supervised}  & FlowNet~\cite{DBLP:journals/corr/FischerDIHHGSCB15}  &               & 8.84          & 9.35         &               &               & 150         \\ \cline{2-8}
		                              & FlowNet2~\cite{Ilg2017}                              &               & 5.739         & 1.8          & 10.41         &               & 100         \\ \cline{2-8}
		                              & LiteFlowNet~\cite{Hui2018}                           &               & 4.903         & 1.4          & 7.62          &               & 88.5        \\ \cline{2-8}
		                              & TVNet~\cite{Fan2018}                                 &               &               &              &               & 0.35          & 83          \\ \cline{2-8}
		                              & PWC-Net~\cite{Sun2018}                               &               & 5.042         & 1.7          & 9.6           & 9.93          & \textbf{30} \\ \cline{2-8}
		                              & DenseNet~\cite{Zhu2018}                              & 4.73          & 10.07         & 11.6         &               &               & 130         \\ \cline{2-8}
		                              & ScopeFlow~\cite{Bar-Haim2020}                        &               & 4.098         & \textbf{1.3} & 6.82          &               &             \\ \cline{2-8}
		                              & SelFlow~\cite{Liu2019}                               &               & 4.262         & 1.5          & 8.42          &               & 90          \\ \cline{2-8}
		                              & SpyNet~\cite{Ranjan2016}                             & \textbf{2.63} & 8.43          & 9.12         &               & \textbf{0.33} & 69          \\ \cline{2-8}
		                              & Yang \textit{et al}\cite{Yang2019}                   &               & \textbf{4.40} &              & \textbf{4.67} & 8.79          &             \\ \hline
		\multirow{5}{*}{Unsupervised} & Yu \textit{et al}\cite{Yu2016}                       & 5.3           &               & 11.3         &               &               &             \\ \cline{2-8}
		                              & UnFlow~\cite{Meister2017}                            &               & 4.26          & 7.68         & 14.19         &               & 120         \\ \cline{2-8}
		                              & Wang \textit{et al}\cite{Wang2018}                   & 3.3           & 9.08          & 4.2          & 31.2          &               &             \\ \cline{2-8}
		                              & GeoNet~\cite{Yin2018}                                &               &               &              & 10.81         &               &             \\ \cline{2-8}
		                              & DFNet~\cite{Zou2018}                                 &               &               & 8.89         & 25.7          &               &             \\ \hline
	\end{tabular}
	\caption{Comparative table as reported by each paper on the tested datasets, times are taken from the KITTI-2015 website}
\end{table*}

\section{Particles and Flow Tracking}
\label{sec:particles_and_flow}

Although the tasks for optical flow and particle tracking are quite
similar, the developed solutions are different. The difference is that
particle tracking is regularly done in controlled laboratory
environments which allows for specialized setups.

Flows are studied by ``seeding'' them with some kind of tracer
particle, which means that both of the particle and the flow tracking
are done in similar matters.

As was mentioned in the introduction, the traditional techniques for
particle tracking are: Particle Image Velocity
(PIV)~\cite{Willert1991}, Bubble Image Velocimetry
(BIV)~\cite{Ryu2005}, and Particle Tracking Velocimetry
(PTV)~\cite{Maas1993}. These three area based on projecting a laser
sheet twice with a know interval between both projection, a
correlation algorithm is then used to match both of the pictures
taken. The depth factor in these come from projecting the laser sheet
at different positions above the object to be studied. The main
disadvantage of this technique is that the usage of correlation to
determine the movement between two frames, since it provides a sparse
map of the flow, and is has difficulty with detecting fast
motions. The method still remains in large usage such as shown
in~\cite{Bung2016a} and~\cite{Rossi2016}.

Two other techniques are the usage of Schlieren~\cite{Jonassen2006}
and Shadowgraph techniques~\cite{etde_20240965} which allow for the
visualization of the change in the flow by using optic techniques,
since the change in the flow will represent a change in refraction
this allows this visualization of the change. \cite{Hayasaka2016}
and~\cite{Smith2017} show the usage of the Schlieren technique in
order to highlight the movement in the flow.

% Interferometry visualization
% laser Doopler velocumetriy

Although traditionally the methods mentioned above have proven
successful in the confined environments work has also been done in
order to test the possible usage of computer vision algorithms in to
study these particles.

% Lucas Kanade

The Lucas-Kanade algorithm is the most popular traditional algorithm
and is available in open source libraries, this allows for a widespread
usage. In~\cite{Abrishami2015} it is used as part of a processing
pipeline in the detection of device micrographs and works well for
local movements but requires global correction before using the LK
methods. Its also used in~\cite{Rossi2019} to determine the velocity
vectors and re-suspension rate. And in~\cite{Lin2019} for water surface
velocimetry with thermal cameras.

It as also complemented with HS~\cite{Kramer2019} to study aerated flows
and with the Farneback~\cite{friman2006bayesian} to study high
velocity air water skimming above a stepped step
chute~\cite{Zhang2018}.

The HS method has also been used to study dust re-suspension
in~\cite{Malizia2017} and for velocity determination in hydraulic
structure models~\cite{Bung2016}.

Optics techniques are also used to improve the visualization before
performing the optical flow in~\cite{Malizia2018}.

% LK + HS + Feature Matching

Other work has also used LK and HS as an starting point before
applying Feature Matching algorithms to track the movement of the
``seed'' particles in~\cite{Malizia2019} and~\cite{Tauro2018}.

% Wavelete

A techniques based on wavelets was also proposed by~\cite{Schmidt2019}
in order to study optical flow and applied to vorticity fields.

% TVL1

Use of deep learning techniques has started to appear some related
tasks such as the usage of TV-L1 in~\cite{StadlerAntonandIke} to
detect and follow the smoke flow.

\section{Conclusions}
\label{sec:conclusions}
%  The paper should end with a section in which the state-of-the art
%  is summarized and future trends in research highlighted.

Work has been developed in CNNs for optical flow over the last few years
thanks to the availability of public datasets, these however introduce
biases for the performance into the network since these datasets are
specialized, ScopeFlow currently represents the state-of-the-art for
this category.

Unsupervised methods have a lower performance but are more general
since they don't require a labeled dataset, here GeoNet has the
highest performance.

The study of optical flow for fluids and particles often used to track
traditionally rely on optical methods and correlation algorithms to
determine the direction of the flow. In the last few years work has
started to incorporate traditional optical flow techniques to provide
a dense optical flow.

Deep learning methods haven't been used by particle tracking
pipelines. This task is ideal for these methods since they are limited
in scope and avoid many of the disadvantages in more general
environments such as occlusions and large displacements.

\printbibliography

\end{document}

